var chai = require("chai");
var expect = chai.expect;
var manip = require("../app/methods/JSONmanip");
var testVar = require("./testJSON.var");


describe("JSON transformation", function () {
  describe("JSON restructuring", function () {
    it("removes duplicates and adds moves to the correct pokemon array", function () {
      var restructured = []
      restructured = manip.filterJSON(testVar.test1Data, true);
      expect(restructured).to.deep.equal(testVar.test1Result);
    });
  });

  describe("JSON fusion", function () {
    it("fuses two JSON and removes usable moves according to moves already in the possession of a pokemon", function () {
      var restructured = []
      restructured = manip.fuseJSON(testVar.test2Data1, testVar.test2Data2);
      expect(restructured).to.deep.equal(testVar.test2Result);
    });
  });
});