var expect = require("chai").expect;
var request = require("request");

describe("Server responses?", function() {

    describe("Recieving the data", function() {
        var url = "http://localhost:3000/checkLogin";

        it("returns negative answer of logging", function(done) {
            request.get(url, function(error, response, body) {
                var hey = JSON.parse(body);
                expect(hey.res).to.equal(false);
                done();
            });
        });
    });
});

describe("Showing the friend list? ", function() {
    var url = "http://localhost:3000/allfriends";
    it("returns false status", function(done) {
        request.get(url, function(error, response, body) {
            var hey = JSON.parse(body);
            expect(hey.rep).to.equal(false);
            done();
        });
    });

});

describe("Showing pikachu ", function() {
    var url = "http://localhost:3000/allpokemon";
    it("returns a pokemon", function(done) {
        request(url, function(error, res, body) {
            var hey = JSON.parse(body)
            expect(hey[24]).to.be.an('object').that.deep.includes({pokeName: 'pikachu'});
            if (error) {
                return console.error('this failed:', error);
            }
            done();
        });
    });

});