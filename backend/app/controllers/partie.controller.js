const Partie = require("../models/partie.model.js");

exports.create = (req, res) => {
    // Validate request
    if (!req.body) {
        res.status(400).send({
            message: "Content can not be empty!"
        });
    }

    // add game to db
    Partie.create(req.session.uid, req.body.pseudo, (err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Pokemon."
            });
        else res.send(data);
    });
};

exports.getDemandes = (req, res) => {
    console.log("Is logged in: " + req.session.loggedIn);
    if (req.session.loggedIn) {
        Partie.getDemandes(req.session.uid, (err, data) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.send({ res: true, body: data });
                } else {
                    res.status(500).send({
                        message: "Error retrieving games with id " + req.session.uid
                    });
                }
            } else {
                res.send({ res: true, body: data });
            }
        });
    } else {
        res.send({ res: false, body: 'You must be logged in to see your games !' });
        res.end();
    }
};

exports.accept = (req, res) => {
    if (req.session.loggedIn) {
        Partie.accept(req.body.id_partie, (err, data) => {
            if (err)
                res.status(500).send({
                    message: err.message || "Some error occurred while accepting the challenge."
                });
            else res.send(data);
        });
    }
};

exports.getCombatTeam = (req, res) => {
  if (req.session.loggedIn) {
      Partie.getCombatTeam(req.body.id_partie, req.session.uid, (err, data1) => {
          if (err)
              res.status(500).send({
                  message: err.message || "Some error occurred while getting combat team."
              });
          else {
            Partie.getCombatEnemyTeam(req.body.id_partie, req.session.uid, (err, data2) => {
                if (err)
                    res.status(500).send({
                        message: err.message || "Some error occurred while getting enemy combat team."
                    });
                else res.send({self : data1, enemy: data2});
            });
          }
      });
  } else{
      res.send({res: false, body: "blaa"});
  }
};

exports.updatePartie = (req, res) => {
    console.log(req.body);
    if (req.session.loggedIn) {
        Partie.updatePartieTour(req.body.id_partie, req.body.winner,  (err, data1) => {
            if (err)
                res.status(500).send({
                    message: err.message || "Some error occurred while updating pokemon state."
                });
            else {
                Partie.updatePartie(req.body.pokemon1.id_etat, req.body.pokemon1.active, req.body.pokemon1.stats, req.body.pokemon1.moves,  (err, data2) => {
                    if (err)
                        res.status(500).send({
                            message: err.message || "Some error occurred while updating pokemon state."
                        });
                    else {
                        Partie.updatePartie(req.body.pokemon2.id_etat, req.body.pokemon2.active, req.body.pokemon2.stats, req.body.pokemon2.moves,  (err, data3) => {
                            if (err)
                                res.status(500).send({
                                    message: err.message || "Some error occurred while updating pokemon state."
                                });
                            else if(req.body.nextPokemon != -1){
                                console.log(req.body.nextPokemon, req.body.pokemon2.id_etat);
                                Partie.activateNextPokemon(req.body.nextPokemon, req.body.pokemon2.id_etat,  (err, data4) => {
                                    if (err)
                                        res.status(500).send({
                                            message: err.message || "Some error occurred while updating pokemon state."
                                        });
                                    res.send(data4);
                                });
                            } else{
                                res.send(data3)
                            }
                        });
                    }
                });
            }
        });
    }
};

exports.refuse = (req, res) => {
    if (req.session.loggedIn) {
        Partie.refuse(req.body.id_partie, (err, data) => {
            if (err)
                res.status(500).send({
                    message: err.message || "Some error occurred while deleting the challenge."
                });
            else res.send(data);
        });
    }
};

exports.eff = (req, res) => {
    if (req.session.loggedIn) {
        Partie.efficacite(req.body.typeNous, req.body.typeEux, (err, data) => {
            if (err)
                res.status(500).send({
                    message: err.message || "Some error occurred while deleting the challenge."
                });
            else res.send(data);
        });
    }
};

exports.getTables = (req, res) => {
    Partie.getTables((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Some error occurred while accepting the challenge."
            });
        else res.send(data);
    });
};