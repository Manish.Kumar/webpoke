const Joueur = require("../models/joueur.model.js");

exports.auth = (req, res) => {
    Joueur.auth(req.body.email, req.body.mdp, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: "Invalid email or password"
                });
            } else {
                res.status(500).send({
                    message: "Error checking authentication "
                });
            }
        } else {
            req.session.loggedIn = true;
            req.session.username = req.body.email;
            req.session.pseudo = data[0]['pseudo'];
            req.session.uid = data[0]['id_joueur'];
            console.log("UID : " + req.session.uid);
            console.log("Session ID: " + req.sessionID);
            res.send({ res: true, body: data[0]['pseudo'] });
        }
    });
};

exports.desAuth = (req, res) => {
    req.session.destroy((err) => {
        res.redirect('/')
    });
}

exports.checkLogin = (req, response) => {
    if (req.session.loggedIn) {
        response.send({ res: true, body: req.session.pseudo, uid: req.session.uid });
    } else {
        response.send({ res: false, body: 'You are not logged in' });
        console.log("Session ID: ", req.sessionID);
    }
    response.end();
};

exports.amiList = (req, res) => {
    if (req.session.loggedIn) {
        Joueur.amiListe(req.session.uid, (err, pseudo) => {
            if (err) {
                if (err.kind === "not_found") {
                    res.send({ rep: false, res: pseudo });
                } else {
                    res.status(500).send({
                        message: "Error retrieving Friends with id " + req.session.uid
                    });
                }
            } else {
                res.send({ rep: true, res: pseudo });
            }
        });
    } else {
        res.send({ rep: false, res: 'You must be logged in to see friends !' });
        res.end();
    }
};

exports.allJoueur = (req, res) => {
    if (req.session.loggedIn) {
        Joueur.getAll(req.session.uid, (err, data) => {
            if (err)
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving Players."
                });
            else res.send(data);
        });
    }
};

exports.newami = (req, res) => {
    if (req.session.loggedIn) {
        Joueur.addAmi(req.session.uid, req.body.id_joueur2, (err, data) => {
            if (err)
                res.status(500).send({
                    message: err.message || "Some error occurred while sending the friend request."
                });
            else res.send(data);
        });
    }
};

exports.accept = (req, res) => {
    if (req.session.loggedIn) {
        Joueur.accept(req.session.uid, req.body.id_joueur2, (err, data) => {
            if (err)
                res.status(500).send({
                    message: err.message || "Some error occurred while accepting the friend request."
                });
            else res.send(data);
        });
    }
};

exports.refuse = (req, res) => {
    if (req.session.loggedIn) {
        console.log(req.body.id_joueur2)
        Joueur.refuse(req.session.uid, req.body.id_joueur2, (err, data) => {
            if (err)
                res.status(500).send({
                    message: err.message || "Some error occurred while deleting the friend request."
                });
            else res.send(data);
        });
    }
};

exports.wins = (req, res) => {
    Joueur.wins((err, data) => {

        if (err)
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving Pokemon."
            });
        else res.send(data);
    });
};