const Pokemon = require("../models/pokemon.model.js");

exports.create = (req, res) => {
    // Validate request
    if (req.session.loggedIn) {
        if (!req.body) {
            res.status(400).send({
                message: "Content can not be empty!"
            });
        }

        // Create a team pokemon
        const pokemon = new Pokemon({
            id_pokemon: req.body.id_pokemon,
            id_joueur: req.session.uid,
            courant: 1
        });

        // add pokemon to team in the database
        Pokemon.create(pokemon, (err, data) => {
            if (err)
                res.status(500).send({
                    message: err.message || "Some error occurred while creating the Pokemon."
                });
            else res.send(data);
        });
    } else{
        res.send({res: false, body: "Please login to use this feature !"});
    }
};

exports.findAll = (req, res) => {
    Pokemon.getAll((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving Pokemon."
            });
        else res.send(data);
    });
};

exports.getTeamUtil = (req, res) => {
    Pokemon.getTeamUtil(req.body.uid, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.send({ res: true, body: data });
            } else {
                res.status(500).send({
                    message: "Error retrieving Team with id " + req.body.uid
                });
            }
        } else {

            res.send({ res: true, body: data });
        }
    });
};

exports.updateMove = (req, res) => {
    Pokemon.updateMove(req.body.nMove, req.body.id_team, req.body.id_move, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.send({ res: false, body: data });
            } else {
                res.status(500).send({
                    message: "Error updating moves",
                    received: req.body
                });
            }
        } else {

            res.send({ res: true, body: data });
        }
    });
};

exports.remove = (req, res) => {
    Pokemon.remove(req.body.id_team, (err, data) => {
        if (err) {
            if (err.kind === "not_found") {
                res.status(404).send({
                    message: `Not found Team with user id ${req.session.uid} and Pokemon id ${req.body.id_pokemon}`
                });
            } else {
                res.status(500).send({
                    message: "Could not delete Pokemon from team with user id " + req.session.uid + " and Pokemon id " + req.body.id_pokemon
                });
            }
        } else res.send({ message: `Pokemon was deleted successfully from Team!` });
    });
};

exports.popular = (req, res) => {
    Pokemon.mostUsed((err, data) => {
        if (err)
            res.status(500).send({
                message: err.message || "Some error occurred while retrieving the bests."
            });
        else res.send(data);
    });
};