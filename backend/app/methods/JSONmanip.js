var _ = require('underscore');

exports.filterJSON = function(data, type) {
    var moveNom = "movesPoss";
    if (type) moveNom = "movesUtil";
    var dataSet = new Set();
    var filteredData = [];
    var i = -1;
    Object.keys(data).forEach(function(key) {
        var switcher = false;
        var temp = {};
        var move = {};
        Object.keys(data[key]).forEach(function(key2) {
            if (key2 == "id_move") switcher = true;
            if (switcher) move[key2] = data[key][key2];
            else { temp[key2] = data[key][key2]; };
        });
        temp[moveNom] = [];
        if (dataSet.has(data[key]["id_pokemon"])) filteredData[i][moveNom].push(move);
        else {
            i++;
            filteredData.push(temp);
            dataSet.add(data[key]["id_pokemon"]);
            filteredData[i][moveNom].push(move);
        }
    });
    return filteredData;
}

exports.filterJSONCombat = function(data) {
    var separatorsNames = ["types", "stats", "base_stats", "moves"]; // nom des catégories finales
    var separators = ["id_type", "id", "baseStat_id", "id_move"]; // nom des colonnes annonçant le début d'une nouvelle catégorie dans le JSON de base
    var containers = [{}]; // initialisation du container temporaire pour toutes les catégories d'un Pokemon
    separators.forEach(sep => containers.push({}));
    var dataSet = new Set(); // stockage des ids des Pokemon, permet de vérifier rapidement si nouveau Pokemon
    var filteredData = []; // resultat final
    var i = -1; // index courant du JSON final et donc du Pokemon courant
    Object.keys(data).forEach(function(key) {
        var switcher = 0; // switcher entre categories
        containers.forEach((el, index, containers) => containers[index] = {}); // vider les categories pour le prochain element
        Object.keys(data[key]).forEach(function(key2) {
            if (separators.includes(key2)) switcher++;
            containers[switcher][key2] = data[key][key2]; // assigner les elements a chaque categorie
        });
        separatorsNames.forEach(sep => containers[0][sep] = []); // Le premier element du tableau contiendra toutes les categories du tableaux, utile pour un nouveau Pokemon
        if (dataSet.has(data[key]["id_pokemon"])) { //Pokemon deja present dans JSON final
            for(var j = 0; j < separators.length; j++){
                filteredData[i][separatorsNames[j]].push(containers[j+1]); // ajouter les nouveaux elements tableaux des categories au Pokémon
            }
        } else {
            i++; // nouveau Pokemon, donc nouvelle case
            filteredData.push(containers[0]); // assigne le conteneur temporaire
            dataSet.add(data[key]["id_pokemon"]);
            for(var j = 0; j < separators.length; j++){
                filteredData[i][separatorsNames[j]].push(containers[j+1]); // ajouter les elements
            }
        }
    });
    filteredData.forEach(pokemon => {
        separatorsNames.forEach(sep => pokemon[sep] = arrUnique(pokemon[sep])); // eleminer la redondance dans chaque categorie
    });
    return filteredData;
}

exports.filterJSONCombatOld = function(data) {
    var separatorsNames = ["types", "stats", "base_stats", "moves"];
    var separators = ["id_type", "id", "baseStat_id", "id_move"];
    var dataSet = new Set();
    var filteredData = [];
    var i = -1;
    Object.keys(data).forEach(function(key) {
        var switcher = 0;
        var tempArray = {};
        var typesContainer = {};
        var statsContainer = {};
        var baseStatsContainer = {};
        var movesContainer = {};
        Object.keys(data[key]).forEach(function(key2) {
            if (separators.includes(key2)) switcher++;
            switch (switcher) {
                case 1:
                    typesContainer[key2] = data[key][key2];
                    break;
                case 2:
                    statsContainer[key2] = data[key][key2];
                    break;
                case 3:
                    baseStatsContainer[key2] = data[key][key2];
                    break;
                case 4:
                    movesContainer[key2] = data[key][key2];
                    break;
                default:
                    tempArray[key2] = data[key][key2];
            }
            // subContainer[key2] = data[key][key2];
        });
        //tempArray[moveNom] = [];
        separatorsNames.forEach(sep => tempArray[sep] = [])
        if (dataSet.has(data[key]["id_pokemon"])) {
            //filteredData[i][moveNom].push(subContainer)
            filteredData[i][separatorsNames[0]].push(typesContainer);
            filteredData[i][separatorsNames[1]].push(statsContainer);
            filteredData[i][separatorsNames[2]].push(baseStatsContainer);
            filteredData[i][separatorsNames[3]].push(movesContainer);
        } else {
            i++;
            filteredData.push(tempArray);
            dataSet.add(data[key]["id_pokemon"]);
            filteredData[i][separatorsNames[0]].push(typesContainer);
            filteredData[i][separatorsNames[1]].push(statsContainer);
            filteredData[i][separatorsNames[2]].push(baseStatsContainer);
            filteredData[i][separatorsNames[3]].push(movesContainer);
        }
    });
    filteredData.forEach(pokemon => {
        pokemon.types = arrUnique(pokemon.types);
        pokemon.moves = arrUnique(pokemon.moves);
        pokemon.stats = arrUnique(pokemon.stats);
        pokemon.base_stats = arrUnique(pokemon.base_stats);
    });
    return filteredData;
}

exports.fuseJSON = function(dataUtil, dataPoss) {
    var fusedJSON = dataUtil;
    Object.keys(fusedJSON).forEach(function(key) {
        var tempTab = fusedJSON[key]["movesUtil"];
        tempTab = tempTab.filter(value => {
            var contain = false;
            dataPoss[key]["movesPoss"].forEach(value2 => { if (value.id_move == value2.id_move) contain = true });
            return !contain;
        });
        fusedJSON[key]["movesUtil"] = tempTab;
        fusedJSON[key]["movesPoss"] = dataPoss[key]["movesPoss"];
    });
    return fusedJSON;
}

exports.test = function() {
    return true;
}

function arrUnique(arr) {
    var cleaned = [];
    arr.forEach(function(itm) {
        var unique = true;
        cleaned.forEach(function(itm2) {
            if (_.isEqual(itm, itm2)) unique = false;
        });
        if (unique) cleaned.push(itm);
    });
    return cleaned;
}