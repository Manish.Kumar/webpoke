module.exports = app => {
    const pokemon = require("../controllers/pokemon.controller.js");

    // Retrieve all Pokemon
    app.get("/allpokemon", pokemon.findAll);

    // Add Pokemon to a team
    app.post("/addpokemon", pokemon.create);

    // Retrieve a team
    app.post("/getteam", pokemon.getTeamUtil);

    // Remove a pokemon from a team
    app.post("/removepokemon", pokemon.remove);

    app.post("/updatemove", pokemon.updateMove);

    //get the pokemons most present in teams
    app.get("/popular", pokemon.popular);

};