module.exports = app => {
    const joueur = require("../controllers/joueur.controller.js");

    // See if the player is logged in
    app.get("/checkLogin", joueur.checkLogin);

    // Tell if the person is connected
    app.post("/auth", joueur.auth);

    // Retrieve all Friends
    app.get("/allfriends", joueur.amiList);

    // Retrieve all Players
    app.get("/alljoueurs", joueur.allJoueur);

    // Add a friend
    app.post("/newami", joueur.newami);

    // Friend accepted
    app.post("/yayami", joueur.accept);

    //Friend declined
    app.post("/nayami", joueur.refuse);

    // Logout
    app.get("/logout", joueur.desAuth);

    //get the descendent wins
    app.get("/winners", joueur.wins);

};