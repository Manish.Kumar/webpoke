module.exports = app => {
    const partie = require("../controllers/partie.controller.js");

    // Add request for game
    app.post("/addpartie", partie.create);

    app.get("/getparties", partie.getDemandes);

    app.post("/acceptpartie", partie.accept);

    app.post("/refusepartie", partie.refuse);

    app.post("/getCombatTeam", partie.getCombatTeam);
    
    app.post("/geteff", partie.eff);

    app.post("/updatepartie", partie.updatePartie);

    //////////
    app.get("/gettables", partie.getTables);

};