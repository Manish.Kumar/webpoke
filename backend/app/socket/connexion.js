const socketIO = require('socket.io')
module.exports.listen = function(app) {
    var connectedUsers = [];
    var usersMap = new Map();
    const io = socketIO.listen(app)
    var numUsers = 0;
    io.on('connection', socket => {
        console.log('New client connected')
        var addedUser = false;


        socket.on('add user', (username) => {
            if (addedUser) return;
            console.log('add user event = ' + username);
            // we store the username in the socket session for this client
            socket.username = username;
            ++numUsers;
            addedUser = true;
            connectedUsers.push(username);
            usersMap.set(username, socket.id)

            console.log("new user added, connected users : " + connectedUsers);

            // echo globally (all clients) that a person has connected
            socket.broadcast.emit('user joined', {
                username: username,
                numUsers: numUsers
            });
        });
        socket.on('get active users', () => {
            var temp = [];
            var user = undefined;
            Array.from(usersMap.keys()).map(key => {
                //console.log(key);
                if (usersMap.get(key) == socket.id) { user = key };
            });
            connectedUsers.forEach(curUser => { if (curUser != user) temp.push(curUser); });
            io.to(socket.id).emit('send list', temp);
        });

        socket.on('new-message', (message) => {
            io.emit('message', { text: message });
            console.log('new message event' + message);
        });

        socket.on('disconnect', () => {
            console.log(usersMap);
            console.log("socket id : " + socket.id);
            var disconnectedUser = undefined;
            Array.from(usersMap.keys()).map(key => {
                //console.log(key);
                if (usersMap.get(key) == socket.id) { disconnectedUser = key };
            });
            console.log('user disconnected : ' + disconnectedUser);
            if (disconnectedUser != undefined) {
                for (let i = 0; i < connectedUsers.length; i++) {
                    //console.log(disconnectedUser + " " + connectedUsers[i]);
                    if (connectedUsers[i] == disconnectedUser) connectedUsers.splice(i, 1);
                }
                usersMap.delete(disconnectedUser);
            }
            console.log('post deletion : ');
            console.log(connectedUsers);
            console.log(usersMap);
            // echo globally that this client has left
            socket.broadcast.emit('user left', socket.username);
        });

        socket.on('battle me', (challenger, guest) => {
            console.log(challenger + " " + guest + " " + usersMap.get(guest));
            io.to(usersMap.get(guest)).emit('demande combat', challenger);
        });

        socket.on('challenge accepted', (guest, challenger) => {
            console.log(challenger + " " + guest + " " + usersMap.get(guest));
            io.to(usersMap.get(challenger)).emit('challenge accepted', guest);
        });

        socket.on('battle data', (to, data) => {
            io.to(usersMap.get(to)).emit('personal battle data', data);
        });
    });
};