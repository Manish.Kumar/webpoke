const sql = require("./db.js");
const JSONmanip = require("../methods/JSONmanip");
const { result } = require("underscore");

// constructor
const Pokemon = function(pokemon) {
    this.id_pokemon = pokemon.id_pokemon;
    this.id_joueur = pokemon.id_joueur;
    this.courant = pokemon.courant;
};


Pokemon.getAll = result => {
    sql.query("SELECT * FROM pokemon", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        //console.log("all good, here is the first Pokemon: ", res[0]);
        result(null, res);
    });
};

Pokemon.mostUsed = result => {
    sql.query("SELECT pokeName, COUNT(*) as number FROM pokemon, team WHERE team.id_pokemon = pokemon.id_pokemon AND courant = 1 GROUP BY team.id_pokemon ORDER BY number DESC", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        result(null, res);
    });
};


// Pokemon.getTeamUtil = (joueur_id, result) => {
//     sql.query('SELECT p.*, m.* FROM pokemon as p, team, moveutil as mu, move as m WHERE id_joueur= ? AND p.id_pokemon=team.id_pokemon AND mu.move_id=m.id_move AND mu.pokemon_id=p.id_pokemon;', [joueur_id], (err, res) => {
//         if (err) {
//             console.log("error: ", err);
//             result(err, null);
//             return;
//         }

//         if (res.length) {
//             //console.log("found team: ", res);
//             result(null, res);
//             return;
//         }

//         // not found team with the id
//         result({ kind: "not_found" }, null);
//     });
// };

Pokemon.getTeamUtil = (joueur_id, result) => {
    //BEGIN; INSERT INTO team (courant ,id_joueur, id_pokemon) VALUES(1, 0, 39); INSERT INTO moveposs (id_move, id_team) SELECT mu.move_id, LAST_INSERT_ID() FROM moveutil as mu WHERE mu.pokemon_id=39 LIMIT 4; COMMIT;
    sql.beginTransaction(function(err) {
        if (err) { throw err; }
        sql.query('SELECT team.id_team, p.*, m.* FROM pokemon as p, team, moveutil as mu, move as m WHERE id_joueur= ? AND p.id_pokemon=team.id_pokemon AND team.courant=1 AND mu.move_id=m.id_move AND mu.pokemon_id=p.id_pokemon ORDER BY team.id_team;', [joueur_id], (err, res1) => {
            if (err) {
                console.log("error: ", err);
                sql.rollback(function() {
                    throw err;
                });
                result(err, null);
                return;
            }

            if (!res1.length) {
                //console.log("found team: ", res);
                result({ kind: "not_found" }, null);
                return;
            }

            //console.log(res1);
            var dataUtil = JSONmanip.filterJSON(res1, true);

            sql.query('SELECT t.id_team, p.*, m.* FROM pokemon as p, moveposs as mp INNER JOIN move as m ON mp.id_move=m.id_move RIGHT JOIN team as t ON mp.id_team=t.id_team WHERE id_joueur= ? AND p.id_pokemon=t.id_pokemon AND t.courant=1 ORDER BY t.id_team;', [joueur_id], (err, res2) => {
                if (err) {
                    console.log("error: ", err);
                    sql.rollback(function() {
                        throw err;
                    });
                    result(err, null);
                    return;
                }
                sql.commit(function(err) {
                    if (err) {
                        console.log("error: ", err);
                        sql.rollback(function() {
                            throw err;
                        });
                        result(err, null);
                        return;
                    }

                    if (!res2.length) {
                        //console.log("found team: ", res);
                        result({ kind: "not_found" }, null);
                        return;
                    }

                    var dataPoss = JSONmanip.filterJSON(res2, false);
                    var fusedData = JSONmanip.fuseJSON(dataUtil, dataPoss);
                    console.log('Transaction Complete.');
                    //console.log(fusedData);
                    result(null, fusedData);
                    //sql.end();
                });
            });
        });
    });
};

Pokemon.getTeamPoss = (joueur_id, result) => {
    sql.query('SELECT p.*, m.* FROM pokemon as p, team, moveposs as mp, move as m WHERE id_joueur= ? AND p.id_pokemon=team.id_pokemon AND mp.id_move=m.id_move AND mp.id_team=team.id_team;', [joueur_id], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            //console.log("found team: ", res);
            result(null, res);
            return;
        }

        // not found team with the id
        result({ kind: "not_found" }, null);
    });
};

Pokemon.updateMove = (nMove, id_team, id_move, result) => {
    sql.query('UPDATE moveposs SET id_move=? WHERE id_team=? AND id_move=?;', [nMove, id_team, id_move], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.affectedRows == 0) {
            // not found team Pokemon with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("updated move with id: ", id_move, " from team pokemon with id: ", id_team, " with new id : ", nMove);
        result(null, res);
    });
}

Pokemon.create = (newPokemon, result) => {
    //BEGIN; INSERT INTO team (courant ,id_joueur, id_pokemon) VALUES(1, 0, 39); INSERT INTO moveposs (id_move, id_team) SELECT mu.move_id, LAST_INSERT_ID() FROM moveutil as mu WHERE mu.pokemon_id=39 LIMIT 4; COMMIT;
    sql.beginTransaction(function(err) {
        if (err) { throw err; }
        sql.query('INSERT INTO team SET ?', newPokemon, function(err, res) {
            if (err) {
                console.log("error: ", err);
                sql.rollback(function() {
                    throw err;
                });
                result(err, null);
                return;
            }

            sql.query('INSERT INTO moveposs (id_move, id_team) SELECT mu.move_id, ? FROM moveutil as mu WHERE mu.pokemon_id=? LIMIT 4', [res.insertId, newPokemon.id_pokemon], function(err, result) {
                if (err) {
                    console.log("error: ", err);
                    sql.rollback(function() {
                        throw err;
                    });
                    result(err, null);
                    return;
                }
                sql.commit(function(err) {
                    if (err) {
                        console.log("error: ", err);
                        sql.rollback(function() {
                            throw err;
                        });
                        result(err, null);
                        return;
                    }
                    console.log('Transaction Complete.');
                    //sql.end();
                });
            });
            result(null, { id: res.insertId, ...newPokemon });
        });
    });
};


Pokemon.remove = (id_team, result) => {
    sql.query('UPDATE team SET courant=0 WHERE id_team=?', [id_team], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        if (res.affectedRows == 0) {
            // not found team Pokemon with the id
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted Pokemon with team id: ", id_team);
        result(null, res);
    });
};


module.exports = Pokemon;