const sql = require("./db.js");

// constructor
const Joueur = function(joueur) {
    this.pseudo = joueur.id_pokemon;
    this.email = joueur.courant;
    this.password = joueur.password;
};


Joueur.auth = (email, password, result) => {
    sql.query('SELECT pseudo, id_joueur FROM joueur WHERE email = ? AND mdp = ?', [email, password], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("Valid authentication: ", res);
            result(null, res);
            return;
        }

        // Invalid email or password
        result({ kind: "not_found" }, null);
    });
};

Joueur.amiListe = (pseudo, results) => {
    sql.query('SELECT DISTINCT pseudo, ami.id_joueur2, ami.id_joueur1, accept FROM ami,joueur WHERE (ami.id_joueur2 = joueur.id_joueur AND ami.id_joueur1 = ?) OR (ami.id_joueur1 = joueur.id_joueur AND ami.id_joueur2 = ?)', [pseudo, pseudo], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("Found friends: ", res);
            results(null, res);
            return;
        }

        results({ kind: "not_found" }, null);
    });
};

Joueur.getAll = (pseudo, result) => {
    sql.query("SELECT pseudo, id_joueur FROM joueur WHERE joueur.id_joueur <> ?", [pseudo], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        result(null, res);
    });
};

Joueur.addAmi = (pseudo, ami, result) => {
    sql.query("INSERT INTO ami VALUES (?,?,0)", [pseudo, ami], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, { id: res.insertId, ...pseudo, ...ami });
    });
};

Joueur.accept = (pseudo, ami, result) => {
    sql.query("UPDATE ami SET accept = 1 WHERE id_joueur1 = ? AND id_joueur2 = ?", [ami, pseudo], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, { id: res.insertId, ...pseudo, ...ami });
    });
};

Joueur.refuse = (pseudo, ami, result) => {
    sql.query("DELETE FROM ami WHERE id_joueur1 = ? AND id_joueur2 = ?", [ami, pseudo], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.affectedRows == 0) {
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted Pokemon with id: ", ami, "from team of user with id: ", pseudo);
        result(null, res);
    });
};

Joueur.wins = (result) => {
    sql.query("SELECT pseudo, win from joueur ORDER BY win DESC", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }
        result(null, res);
    });
};


module.exports = Joueur;