const sql = require("./db.js");
const JSONmanip = require("../methods/JSONmanip");

// constructor
const Partie = function(partie) {
    this.nbTours = partie.nbTours;
    this.joueurTour = partie.joueurTour;
    this.id_joueur1 = partie.id_joueur1;
    this.id_joueur2 = partie.id_joueur2;
};


Partie.create = (uid, pseudo, result) => {
    sql.query('INSERT INTO partie (nbTours, joueurTour, id_joueur1, id_joueur2) SELECT 0, id_joueur, ?, id_joueur FROM joueur WHERE pseudo = ?;', [uid, pseudo], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        console.log("created request: " + pseudo + " " + uid);
        result(null, { id: res.insertId });
    });
};

Partie.getDemandes = (uid, result) => {
    sql.query('SELECT p.*, j.pseudo FROM partie as p, joueur as j WHERE (id_joueur1 = ? AND j.id_joueur = p.id_joueur2) OR (id_joueur2 = ? AND j.id_joueur = p.id_joueur1)', [uid, uid], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            console.log("found games: ", res);
            result(null, res);
            return;
        }

        // not found team with the id
        result({ kind: "not_found" }, null);
    });
};

Partie.accept = (id_partie, resultGlobal) => {
    sql.beginTransaction(function(errTransaction) {
        if (errTransaction) { throw err; }
        sql.query('UPDATE partie SET nbTours=1 WHERE id_partie=?', id_partie, function(err, res) {
            if (err) {
                console.log("error: ", err);
                sql.rollback(function() {
                    throw err;
                });
                resultGlobal(err, null);
                return;
            }
            //INSERT INTO etatpoke (hp, def, atk, active, id_partie, id_team) SELECT p.base_hp, p.base_defense, p.base_attack, 0, 3, team.id_team FROM pokemon as p, partie as pa, team, joueur WHERE pa.id_partie = 3 AND joueur.id_joueur=pa.id_joueur1 AND team.id_joueur=joueur.id_joueur AND p.id_pokemon=team.id_pokemon AND team.courant = 1;
            sql.query('INSERT INTO etatpoke (active, id_partie, id_team) SELECT 0, ?, team.id_team FROM pokemon as p, partie as pa, team, joueur WHERE pa.id_partie = ? AND (joueur.id_joueur=pa.id_joueur1 OR joueur.id_joueur=pa.id_joueur2) AND team.id_joueur=joueur.id_joueur AND p.id_pokemon=team.id_pokemon AND team.courant = 1;', [id_partie, id_partie], function(err, result) {
                if (err) {
                    console.log("error: ", err);
                    sql.rollback(function() {
                        throw err;
                    });
                    resultGlobal(err, null);
                    return;
                }

                sql.query('UPDATE etatpoke SET active=1 WHERE id_etat in (SELECT id_etat FROM (SELECT id_etat FROM etatpoke as ep, partie as p, team as t WHERE p.id_partie=? AND ep.id_partie=p.id_partie AND ep.id_team=t.id_team AND p.id_joueur1=t.id_joueur ORDER BY t.id_team LIMIT 1) as et1) OR id_etat in (SELECT id_etat FROM (SELECT id_etat FROM etatpoke as ep, partie as p, team as t WHERE p.id_partie=? AND ep.id_partie=p.id_partie AND ep.id_team=t.id_team AND p.id_joueur2=t.id_joueur ORDER BY t.id_team LIMIT 1) as et2) ', [id_partie, id_partie], function(err, result) {
                    if (err) {
                        console.log("error: ", err);
                        sql.rollback(function() {
                            throw err;
                        });
                        resultGlobal(err, null);
                        return;
                    }

                    sql.query('INSERT INTO etatstats (id_etat, stat_id, stat_value, effort) SELECT ep.id_etat ,s.stat_id, s.base_stat, s.effort FROM etatpoke as ep, team as t, pokemon as p, stats as s WHERE ep.id_partie = ? AND ep.id_team=t.id_team AND t.id_pokemon=p.id_pokemon AND s.pokemon_id=p.id_pokemon;', [id_partie], function(err, result) {
                        if (err) {
                            console.log("error: ", err);
                            sql.rollback(function() {
                                throw err;
                            });
                            resultGlobal(err, null);
                            return;
                        }

                        sql.query('INSERT INTO etatmove (id_etat, id_move, pp) SELECT ep.id_etat, mp.id_move, m.pp FROM etatpoke as ep, moveposs as mp, move as m, team, partie WHERE partie.id_partie = ? AND ep.id_partie = partie.id_partie AND ep.id_team=team.id_team AND mp.id_team=team.id_team AND mp.id_move=m.id_move;', [id_partie], function(err, result) {
                            if (err) {
                                console.log("error: ", err);
                                sql.rollback(function() {
                                    throw err;
                                });
                                resultGlobal(err, null);
                                return;
                            }
                            sql.commit(function(err) {
                                if (err) {
                                    console.log("error: ", err);
                                    sql.rollback(function() {
                                        throw err;
                                    });
                                    resultGlobal(err, null);
                                    return;
                                }
                                console.log('Transaction Complete !!!!');
                                //sql.end();
                            });
                        });
                    });
                });
            });
            resultGlobal(null, { res: true, id: res.insertId });
        });
    });
    // sql.query("UPDATE partie SET nbTours=1 WHERE id_partie=?", [id_partie], (err, res) => {
    //     if (err) {
    //         console.log("error: ", err);
    //         result(err, null);
    //         return;
    //     }
    //     result(null, {res: true, id: res.insertId});
    // });
};

Partie.getCombatTeam = (id_partie, id_joueur, result) => {
    sql.query("SELECT DISTINCT p.id_pokemon, p.pokeName, ep.id_etat, ep.active, typePoke.id_type, typePoke.nomType as typeNom, gs.id, gs.identifier as statName, es.stat_value, es.effort, s.stat_id as baseStat_id, gsBase.identifier as baseStatName, s.base_stat, m.id_move, m.identifier, m.power, m.accuracy, typeMove.id_type as m_id_type, typeMove.nomType, em.pp, dc.id_dmg_classe, dc.identifier as dmg_classe FROM etatpoke as ep, etatstats as es, etatmove as em, team as t, joueur as j, pokemon as p, move as m, genstate as gs, type as typePoke, type as typeMove, pokemon_type as pty, stats as s, genstate as gsBase, damage_classe as dc WHERE ep.id_partie=? AND ep.id_team=t.id_team AND t.id_joueur=? AND ep.id_etat=es.id_etat AND ep.id_etat=em.id_etat AND t.id_pokemon=p.id_pokemon AND m.id_move=em.id_move AND gs.id=es.stat_id AND p.id_pokemon=pty.pokemon_id AND pty.type_id=typePoke.id_type AND m.type_id=typeMove.id_type AND p.id_pokemon=s.pokemon_id AND gsBase.id=s.stat_id AND m.damage_class_id=dc.id_dmg_classe ORDER BY p.id_pokemon, gs.id, s.stat_id", [id_partie, id_joueur], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            var filteredData = JSONmanip.filterJSONCombat(res);
            result(null, filteredData);
            return;
        }

        // not found combat team with the id
        result({ kind: "not_found" }, null);
    });
}

Partie.getCombatEnemyTeam = (id_partie, id_joueur, result) => {
    sql.query("SELECT DISTINCT p.id_pokemon, p.pokeName, ep.id_etat, ep.active, typePoke.id_type, typePoke.nomType as typeNom, gs.id, gs.identifier as statName, es.stat_value, es.effort, s.stat_id as baseStat_id, gsBase.identifier as baseStatName, s.base_stat, m.id_move, m.identifier, m.power, m.accuracy, typeMove.id_type as m_id_type, typeMove.nomType, em.pp, dc.id_dmg_classe, dc.identifier as dmg_classe FROM etatpoke as ep, etatstats as es, etatmove as em, team as t, joueur as j, pokemon as p, move as m, genstate as gs, type as typePoke, type as typeMove, pokemon_type as pty, stats as s, genstate as gsBase, damage_classe as dc WHERE ep.id_partie=? AND ep.id_team=t.id_team AND t.id_joueur != ? AND ep.id_etat=es.id_etat AND ep.id_etat=em.id_etat AND t.id_pokemon=p.id_pokemon AND m.id_move=em.id_move AND gs.id=es.stat_id AND p.id_pokemon=pty.pokemon_id AND pty.type_id=typePoke.id_type AND m.type_id=typeMove.id_type AND p.id_pokemon=s.pokemon_id AND gsBase.id=s.stat_id AND m.damage_class_id=dc.id_dmg_classe  ORDER BY p.id_pokemon, gs.id, s.stat_id", [id_partie, id_joueur], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }

        if (res.length) {
            var filteredData = JSONmanip.filterJSONCombat(res);
            result(null, filteredData);
            return;
        }

        // not found combat team with the id
        result({ kind: "not_found" }, null);
    });
}

Partie.updatePartieTour = (id_partie, winner, result) => { ////////////??????????????
    if(winner == undefined){
        sql.query('UPDATE partie SET nbTours=nbTours+1, joueurTour = (CASE WHEN joueurTour = id_joueur1 THEN id_joueur2 WHEN joueurTour = id_joueur2 THEN id_joueur1 ELSE joueurTour END) WHERE id_partie=?', id_partie, function(err, res) {
            if (err) {
                console.log("error: ", err);
                result(err, null);
                return;
            }
            // not found combat team with the id
            result(null, { res: true, id: res.insertId });
        });
    } else{
        sql.beginTransaction(function(errTransaction) {
            if (errTransaction) { throw err; }
            sql.query('UPDATE partie SET nbTours=nbTours+1, winner=?, joueurTour = (CASE WHEN joueurTour = id_joueur1 THEN id_joueur2 WHEN joueurTour = id_joueur2 THEN id_joueur1 ELSE joueurTour END) WHERE id_partie=?', [winner, id_partie], function(err, res) {
                if (err) {
                    console.log("error: ", err);
                    sql.rollback(function() {
                        throw err;
                    });
                    result(err, null);
                    return;
                }
                sql.query('UPDATE joueur SET win=win+1 WHERE id_joueur=?', winner, function(err, res) {
                    if (err) {
                        console.log("error: ", err);
                        sql.rollback(function() {
                            throw err;
                        });
                        result(err, null);
                        return;
                    }
                    sql.commit(function(err) {
                        if (err) {
                            console.log("error: ", err);
                            sql.rollback(function() {
                                throw err;
                            });
                            result(err, null);
                            return;
                        }
                        console.log('Transaction Complete for winner !!!!');
                        //sql.end();
                    });
                });
                // not found combat team with the id
                result(null, { res: true, id: res.insertId });
            });
        });
    } 
}

Partie.updatePartie = (id_etat, active, stats, moves, result) => {
    sql.beginTransaction(function(errTransaction) {
        if (errTransaction) { throw err; }
        // sql.query('UPDATE etatpoke SET active=? WHERE id_etat=?', [active, id_etat], function(err, res) {
        //     if (err) {
        //         console.log("error: ", err);
        //         sql.rollback(function() {
        //             throw err;
        //         });
        //         result(err, null);
        //         return;
        //     }

            var sqlStats = [];
            stats.forEach(stat => {
                sqlStats.push(id_etat, stat.id, stat.stat_value);
            });

            sql.query('INSERT INTO etatstats (id_etat, stat_id, stat_value, effort) VALUES (?, ?, ?, 0), (?, ?, ?, 0), (?, ?, ?, 0), (?, ?, ?, 0), (?, ?, ?, 0), (?, ?, ?, 0) ON DUPLICATE KEY UPDATE id_etat=VALUES(id_etat), stat_id=VALUES(stat_id), stat_value=VALUES(stat_value), effort=VALUES(effort);', sqlStats, function(err, res) {
                if (err) {
                    console.log("error: ", err);
                    sql.rollback(function() {
                        throw err;
                    });
                    result(err, null);
                    return;
                }

                var sqlMoves = [];
                moves.forEach(move => {
                    sqlMoves.push(id_etat, move.id_move, move.pp);
                });

                sql.query('INSERT INTO etatmove (id_etat, id_move, pp) VALUES (?, ?, ?), (?, ?, ?), (?, ?, ?), (?, ?, ?) ON DUPLICATE KEY UPDATE id_etat=VALUES(id_etat), id_move=VALUES(id_move), pp=VALUES(pp);', sqlMoves, function(err, res) {
                    if (err) {
                        console.log("error: ", err);
                        sql.rollback(function() {
                            throw err;
                        });
                        result(err, null);
                        return;
                    }
                    sql.commit(function(err) {
                        if (err) {
                            console.log("error: ", err);
                            sql.rollback(function() {
                                throw err;
                            });
                            result(err, null);
                            return;
                        }
                        console.log('Transaction Complete: etatPoke updated');
                        //sql.end();
                    });
                });
                result(null, { res: true, id: res.insertId });
            });
        // });
    });
}

Partie.activateNextPokemon = (next_id_etat, dead_id_etat, result) => {
    sql.beginTransaction(function(errTransaction) {
        if (errTransaction) { throw err; }
        sql.query('UPDATE etatpoke SET active=1 WHERE id_etat=?', [next_id_etat], function(err, res) {
            if (err) {
                console.log("error: ", err);
                sql.rollback(function() {
                    throw err;
                });
                result(err, null);
                return;
            }

            sql.query('UPDATE etatpoke SET active=0 WHERE id_etat=?', [dead_id_etat], function(err, res) {
                if (err) {
                    console.log("error: ", err);
                    sql.rollback(function() {
                        throw err;
                    });
                    result(err, null);
                    return;
                }
            
                sql.commit(function(err) {
                    if (err) {
                        console.log("error: ", err);
                        sql.rollback(function() {
                            throw err;
                        });
                        result(err, null);
                        return;
                    }
                    console.log('Transaction complete : death of pokemon, pokemon switched', next_id_etat, dead_id_etat);
                    //sql.end();
                });
            });

            result(null, res);
        });
    });
}

Partie.refuse = (id_partie, result) => {
    sql.query("DELETE FROM partie WHERE id_partie=?", [id_partie], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        if (res.affectedRows == 0) {
            result({ kind: "not_found" }, null);
            return;
        }

        console.log("deleted game with id: ", id_partie);
        result(null, { res: true, body: res });
    });
};

Partie.efficacite = (typeNous, typeEux, result) => {
    sql.query("SELECT damage_factor from efficacite WHERE damage_type_id = ? AND target_type_id = ?", [typeNous, typeEux], (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(err, null);
            return;
        }
        result(null, { body: res })
    });

};

Partie.getTables = result => {
    sql.query("show tables", (err, res) => {
        if (err) {
            console.log("error: ", err);
            result(null, err);
            return;
        }

        //console.log("all good, here is the first Pokemon: ", res[0]);
        result(null, res);
    });
};

module.exports = Partie;