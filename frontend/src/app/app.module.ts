import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpClientModule } from '@angular/common/http';  
import { ChatService } from './chat.service';
import { AccountSyncService } from "./services/account-sync/account-sync.service";
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { PokedexMenuComponent } from './pokedex-menu/pokedex-menu.component';
import { TeamMenuComponent } from './team-menu/team-menu.component';
import { FriendsMenuComponent } from './friends-menu/friends-menu.component';
import { BattleMenuComponent } from './battle-menu/battle-menu.component';
import { AccountMenuComponent } from './account-menu/account-menu.component';
import { ClickOutsideDirective } from './click-outside.directive';
import { ProfilComponent } from './profil/profil.component';
import { HomeComponent } from './home/home.component';
import { OnlinePlayersComponent } from './sub-components/online-players/online-players.component';
import { NotifierModule } from "angular-notifier";
import { BattleComponent } from './battle/battle.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { LeaderboardComponent } from './leaderboard/leaderboard.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    PokedexMenuComponent,
    TeamMenuComponent,
    FriendsMenuComponent,
    BattleMenuComponent,
    AccountMenuComponent,
    ClickOutsideDirective,
    ProfilComponent,
    HomeComponent,
    OnlinePlayersComponent,
    BattleComponent,
    LeaderboardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NotifierModule,
    NgbModule
  ],
  providers: [
    ChatService,
    AccountSyncService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
