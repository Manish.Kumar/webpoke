import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { __assign } from 'tslib';
import { ChatService } from "../chat.service";

@Component({
  selector: 'app-battle-menu',
  templateUrl: './battle-menu.component.html',
  styleUrls: ['./battle-menu.component.css']
})
export class BattleMenuComponent implements OnInit {

  pseudo: string;
  uid;
  loggedIn: boolean;
  parties: any = [];
  curParties: any = [];
  demandesExt: any = [];
  demandesInt: any = [];

  constructor(private chatservice: ChatService, private http: HttpClient){}
  
  ngOnInit() {
    this.http.get('http://localhost:3000/checkLogin', { responseType: 'json', withCredentials: true}).subscribe( (res: any) => {
      this.loggedIn = res.res;
      this.pseudo = res.body;
      this.uid = res.uid;
      console.log(res);
      if(this.loggedIn){
        this.pageInit();
      }else{
        console.log("not logged in");
      }
    });  
  }

  pageInit(){
    this.http.get('http://localhost:3000/getparties', { responseType: 'json', withCredentials: true}).subscribe((res: any) => {
      if(res.res) {
        this.parties = res.body;
        this.filterParties(res.body);
        console.log(res.body);
      }
      else console.log(res.body);
    });
  }

  filterParties(parties){
    for(let partie of parties){
      if(partie.winner == null){
        if(partie.nbTours == 0){
          if(partie.id_joueur1 == this.uid) this.demandesInt.push(partie);
          else this.demandesExt.push(partie);
        } else{
          this.curParties.push(partie);
        }
      }
    }
  }

  acceptChallenge(partie, i){
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json');
    this.http.post('http://localhost:3000/acceptpartie', {id_partie: partie.id_partie}, { headers: headers, withCredentials: true}).subscribe(
      (response: any) => {
        console.log(response); 
        if(response.res) {
          this.curParties.push(this.demandesExt[i]);
          this.demandesExt.splice(i, 1);
          this.chatservice.challengeAccepted(partie.id_joueur1)
        }
      },
      (error) => console.log(error)
    )  
  }

  refuseChallenge(id_partie, i){
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json');
    this.http.post('http://localhost:3000/refusepartie', {id_partie: id_partie}, { headers: headers, withCredentials: true}).subscribe(
      (response: any) => {
        console.log(response); 
        if(response.res) {
          this.demandesExt.splice(i, 1);
        }
      },
      (error) => console.log(error)
    )  
  }

}
