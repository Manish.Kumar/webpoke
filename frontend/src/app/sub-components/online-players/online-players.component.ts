import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../chat.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'app-online-players',
  templateUrl: './online-players.component.html',
  styleUrls: ['./online-players.component.css']
})
export class OnlinePlayersComponent implements OnInit {

  users = new Set();

  constructor(private httpClient: HttpClient, private chatService: ChatService) { }

  ngOnInit(): void {
    this.chatService.currentUsers.subscribe(users => this.users = users);
  }

  challenge(user){
    if(this.chatService.attaquer(user)){
      this.addPartie(user);
      console.log("sending challenge");
    }
  }

  addPartie(user){
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json');
    this.httpClient.post('http://localhost:3000/addpartie', {pseudo: user}, { headers: headers, withCredentials: true}).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    )  
  }
}
