import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class AccountSyncService {

  private username = new BehaviorSubject(null);
  currentUsername = this.username.asObservable();

  constructor() { }

  updateUsername(username: string) {
    this.username.next(username)
  }

}
