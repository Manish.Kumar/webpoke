import { TestBed } from '@angular/core/testing';

import { AccountSyncService } from './account-sync.service';

describe('AccountSyncService', () => {
  let service: AccountSyncService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AccountSyncService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
