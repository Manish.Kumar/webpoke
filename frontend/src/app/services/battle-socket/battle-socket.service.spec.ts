import { TestBed } from '@angular/core/testing';

import { BattleSocketService } from './battle-socket.service';

describe('BattleSocketService', () => {
  let service: BattleSocketService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BattleSocketService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
