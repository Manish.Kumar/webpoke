import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AccountSyncService } from "../services/account-sync/account-sync.service";
import { ChatService } from "../chat.service";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  currentUser: any = {};
  titre: String;
  chemin: String;
  constructor(private chatService: ChatService, private accountData: AccountSyncService, private http: HttpClient) { }

  ngOnInit(): void {
    this.http.get('http://localhost:3000/checkLogin', { responseType: 'json', withCredentials: true}).subscribe(res => {this.currentUser = res;
      console.log(this.currentUser);
      if(this.currentUser.res){
        this.titre = this.currentUser.body;
        this.chemin = "/profil";
        this.chatService.notify(this.titre);
      }else{
        this.accountData.currentUsername.subscribe(username => this.checkConnexion(username));
      }
    });
  }

  checkConnexion(username){
    if(username == null) {
      this.titre = 'Account';
      this.chemin = "/account";
    }
    else {
      this.titre = username;
      this.chemin = "/profil";
    }
  }
  
}
