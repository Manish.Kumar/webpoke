import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgLocalization } from '@angular/common';

@Component({
  selector: 'app-team',
  templateUrl: './team-menu.component.html',
  styleUrls: ['./team-menu.component.css']
})
export class TeamMenuComponent implements OnInit {

  loggedIn: boolean;
  nbAttacks: Number[] = [1, 2, 3, 4];
  chosenAttacks = new Map();
  pseudo: string;
  uid: Number;
  team: any = [];
  constructor(private http: HttpClient) { }
  
  removePokemon(pokemon){
    const headers = new HttpHeaders()
          .set('Content-Type', 'application/json');
    this.http.post('http://localhost:3000/removepokemon', {id_team: pokemon.id_team} , { headers: headers, withCredentials: true}).subscribe(
      (response) => {console.log(response); this.ngOnInit();},
      (error) => console.log(error)
    )
  }

  pageInit(){
    this.http.post("http://localhost:3000/getteam", {uid: this.uid}, { responseType: 'json', withCredentials: true }).subscribe(
      (response: any) => {
        console.log(response); 
        if(response.res) {
          this.team = response.body;
          this.team.forEach(pokemon => {
            var temp = [];
            pokemon.movesPoss.forEach(move => {
              temp.push(move.identifier);
            });
            this.chosenAttacks.set(pokemon.pokeName, temp);
          });
          console.log(this.team);
        }
        else console.log("error retrieving team");
      },
      (error) => {console.log(error)});
  }

  updateAttacks(pokemon, move, j, k){
    var temp = this.chosenAttacks.get(pokemon.pokeName);
    temp[j] = move.identifier;
    this.chosenAttacks.set(pokemon.pokeName, temp);
    this.http.post("http://localhost:3000/updateMove", {nMove: move.id_move, id_team: pokemon.id_team, id_move: pokemon.movesPoss[j].id_move}, { responseType: 'json', withCredentials: true }).subscribe(
      (response: any) => {},
      (error) => {console.log(error)}
    );
    var tempMove = move;
    pokemon.movesUtil[k] = pokemon.movesPoss[j];
    pokemon.movesPoss[j] = tempMove;

    var tempTab = this.chosenAttacks.get(pokemon.pokeName);
    tempTab[j] = tempMove.identifier;
    this.chosenAttacks.set(pokemon.pokeName, tempTab);

    console.log(this.chosenAttacks);
  }

  showAttacks(pokemon){
    var temp = "";
    this.chosenAttacks.get(pokemon.pokeName).forEach(attack => {
      temp += attack + ", ";
    });
    return temp.slice(0, -1); 
  }

  ngOnInit(): void {
    this.http.get('http://localhost:3000/checkLogin', { responseType: 'json', withCredentials: true}).subscribe( (res: any) => {
      this.loggedIn = res.res;
      this.pseudo = res.body;
      this.uid = res.uid;
      console.log(res);
      if(this.loggedIn){
        this.pageInit();
      }else{
        console.log("not logged in");
      }
    });  
  }

}
