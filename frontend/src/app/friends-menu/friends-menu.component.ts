import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import { ChatService } from '../chat.service';

@Component({
  selector: 'app-friends',
  templateUrl: './friends-menu.component.html',
  styleUrls: ['./friends-menu.component.css']
})
export class FriendsMenuComponent implements OnInit {

  amis: any = [];
  resultat: any = {};
  log: boolean;
  currentAmi: any ={};
  show: boolean;
  profileBody: any;
  joueurs: any = [];
  queryField: FormControl = new FormControl();
  nom: any = {};
  results: any[] = [];
  requests: any = [];
  pseudo: string;
  uid;
  temp;

  constructor(private httpClient: HttpClient, private chatService: ChatService) { }

  displayPlayers(play){
    this.currentAmi = play;
    this.show = true;
    this.httpClient.get("http://localhost:3000/alljoueurs", { responseType: 'json' , withCredentials:true}).subscribe(res => {this.nom = res; this.profileBody = this.nom; console.log(res)})
  }

  addfriend(){
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json');
    this.httpClient.post('http://localhost:3000/newami', { id_joueur2: this.currentAmi.id_joueur}, { headers: headers, withCredentials: true}).subscribe(
      (response: any) => {
        if(response.res){
          this.amis.push(this.requests[this.currentAmi]);
        }
      }
    )
  }


  suggest(data : String){
    if(data.length == 0) return [];
    var newArr = this.joueurs.filter(play => play.pseudo.includes(data));
    console.log(newArr);
    return newArr;
  }  

  ordre(){
    this.httpClient.get('http://localhost:3000/allfriends', { responseType: 'json', withCredentials:true }).subscribe(res => {this.resultat = res; this.isFriend(); console.log(res)})
  }

  isFriend(){
    if(this.resultat.res.length > 0){
    for(let i = 0; i < this.resultat.res.length; i++){
      if(this.resultat.res[i].accept == 1){
        this.amis.push(this.resultat.res[i])
      } else {
      this.requests.push(this.resultat.res[i])
      }
    }
   } 
  }

  accepter(idAmi, i){
    if(idAmi.id_joueur1 == this.uid){
      this.temp = idAmi.id_joueur2
    } else {
      this.temp = idAmi.id_joueur1
    }
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json');
    this.httpClient.post('http://localhost:3000/yayami', { id_joueur2: this.temp}, { headers: headers, withCredentials: true}).subscribe(
      (response: any) => {
        if(response.res){
          this.amis.push(this.requests[i]);
        }
      }
    )
  }

  refuser(idAmi, i){
    if(idAmi.id_joueur1 == this.uid){
      this.temp = idAmi.id_joueur2
    } else {
      this.temp = idAmi.id_joueur1
    }
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json');
    this.httpClient.post('http://localhost:3000/nayami', { id_joueur2: this.temp }, { headers: headers, withCredentials: true}).subscribe(
      (response: any) => {
        if(response.res){
          this.requests.splice(i, 1);
        }
      }
    )  
  }

  challenge(user){
    if(this.chatService.attaquer(user)){
      this.addPartie(user);
      console.log("sending challenge");
    }
  }
  
  addPartie(user){
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json');
    this.httpClient.post('http://localhost:3000/addpartie', {pseudo: user}, { headers: headers, withCredentials: true}).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    )  
  }
  

  liste(){
    this.httpClient.get("http://localhost:3000/alljoueurs", { responseType: 'json', withCredentials:true}).
    subscribe(res => {this.joueurs = res; this.ordre(); console.log(res)})
    this.queryField.valueChanges
    .debounceTime(200)
    .distinctUntilChanged()
    .subscribe(query => this.results = this.suggest(query));
  }


  ngOnInit(): void {
    this.httpClient.get("http://localHost:3000/checkLogin", { responseType: 'json', withCredentials: true}).subscribe( (res: any) => {
      this.log = res.res;
      this.pseudo = res.body;
      this.uid = res.uid;
      this.liste();
    }); 
  }
}
