import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormControl } from '@angular/forms';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';


@Component({
  selector: 'app-pokedex',
  templateUrl: './pokedex-menu.component.html',
  styleUrls: ['./pokedex-menu.component.css']
})
export class PokedexMenuComponent implements OnInit {

  temp: String;
  pokemons: any = [];
  currentPoke: any = {};
  results: any[] = [];
  queryField: FormControl = new FormControl();
  show: boolean = false;
  showPoke: boolean = false;
  profileImage: String = "";
  flavor_text: any = {};
  profileBody: any;


  constructor(private httpClient: HttpClient) { }

  suggest(data : String){
    if(data.length == 0) return [];
    var newArr = this.pokemons.filter(poke => poke.pokeName.includes(data));
    console.log(newArr);
    return newArr;
  }

  displayPoke(pokemon){
    this.currentPoke = pokemon;
    console.log(pokemon.pokeName + " " + pokemon.id_pokemon);
    this.showPoke = true;
    this.profileImage = "https://pokeres.bastionbot.org/images/pokemon/" + pokemon.id_pokemon + ".png";
    this.httpClient.get("https://pokeapi.co/api/v2/pokemon-species/" + pokemon.id_pokemon + "/", {responseType:'json'}).
    subscribe(res => {this.flavor_text = res; this.updateBody();})
    console.log(this.profileImage);
  }

  updateBody(){
    this.profileBody = this.flavor_text.flavor_text_entries.filter(entry => entry.language.name == "en")[0].flavor_text
  }

  addpokemon(){
    const headers = new HttpHeaders()
    .set('Content-Type', 'application/json');
    this.httpClient.post('http://localhost:3000/addpokemon', {id_pokemon: this.currentPoke.id_pokemon}, { headers: headers, withCredentials: true}).subscribe(
      (response) => console.log(response),
      (error) => console.log(error)
    )  
  }

  ngOnInit(): void {
    this.httpClient.get("http://localhost:3000/allpokemon", { responseType: 'json' }).
    subscribe(res => {this.pokemons = res; console.log(res);})
    this.queryField.valueChanges
    .debounceTime(200)
    .distinctUntilChanged()
    .subscribe(query => this.results = this.suggest(query));
  }
}
