import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ChatService } from '../chat.service';
import { AccountSyncService } from "../services/account-sync/account-sync.service";
import { Router } from '@angular/router';



@Component({
  selector: 'app-account-menu',
  templateUrl: './account-menu.component.html',
  styleUrls: ['./account-menu.component.css']
})
export class AccountMenuComponent implements OnInit {
  form: FormGroup;

  constructor(private accountData: AccountSyncService, private chatService: ChatService, private http: HttpClient, public fd: FormBuilder, private router: Router) { 
    this.form = this.fd.group({
    name: [''],
    password: ['']
  })}

  ngOnInit(): void {}

  submitForm() {
    const headers = new HttpHeaders()
            .set('Content-Type', 'application/json');
    const resultat = {email:this.form.get('name').value , mdp: this.form.get('password').value };
    this.http.post('http://localhost:3000/auth', resultat , { headers: headers, withCredentials: true}).subscribe(
      (response) => {console.log(response); this.verif(response)},
      (error) => console.log(error)
    ); 
    this.router.navigate(['/pokedex']);
  }

  verif(response){
    if(response.res){
      this.chatService.notify(response.body);
      this.accountData.updateUsername(response.body);
    } else {
      console.log("wrong user");
    }
  }
}