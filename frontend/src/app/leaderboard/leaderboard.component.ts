import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-leaderboard',
  templateUrl: './leaderboard.component.html',
  styleUrls: ['./leaderboard.component.css']
})
export class LeaderboardComponent implements OnInit {

  constructor(private httpClient: HttpClient) { }

  resultat: any = [];
  poke:any = [];

  ngOnInit(): void {
    this.httpClient.get('http://localhost:3000/winners', { responseType: 'json', withCredentials:true }).subscribe(res => {this.resultat = res})
    this.httpClient.get('http://localhost:3000/popular', { responseType: 'json', withCredentials:true }).subscribe(res => {this.poke = res})
  }

}
