import * as io from 'socket.io-client';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { NotifierService } from "angular-notifier";

@Injectable({
  providedIn: 'root',
})
export class ChatService {

  private readonly notifier: NotifierService;
  private url = 'http://localhost:3000';
  private socket;
  private tempUsers = new Set<string>();
  private users = new BehaviorSubject(new Set<string>());
  currentUsers = this.users.asObservable();
  private pseudo = undefined;

  constructor(notifierService: NotifierService) {
    this.notifier = notifierService;
    this.socket = io(this.url);
    this.onInit();
  }

  private onInit(){
    this.getChallenges();
    this.onlineUsers();
  }

  private getChallenges(){
    this.recevoirChallenge().subscribe((challenger: string) =>{
      this.notifier.notify("success", challenger + " wants to battle !");
      console.log("challenge from : " + challenger);
    });
    this.recevoirChallengeAccepted().subscribe((guest: string) =>{
      this.notifier.notify("success", guest + " accepted challenge !");
      console.log("challenge accepted by : " + guest);
    });
  }

  private onlineUsers(){
    this.recevoirListe().subscribe((liste: string[]) =>{
      this.tempUsers = new Set(liste);
      this.users.next(new Set(liste));
      console.log(liste);
      console.log(this.users);
    });
    this.getNewUsers().subscribe((newUser: string) => {
      this.tempUsers.add(newUser); 
      this.users.next(this.tempUsers);
    });
    this.userLeft().subscribe((userLeft: string) => {
      this.tempUsers.delete(userLeft);
      this.users.next(this.tempUsers);
    });
  }

  public getPseudo(){
    return this.pseudo;
  }

  public sendMessage(message) {this.socket.emit('new-message', message);}

  public getNewUsers() {
    let observable = new Observable(observer => { 
      this.socket.on('message', (data) => {
        observer.next(data.text); 
      });
      this.socket.on('user joined', (data) => {
        console.log(data.username + ' joined');
        observer.next(data.username);
      });
      return () => {this.socket.disconnect();}; 
    })
    return observable;
  }

  public userLeft(){
    let observable = new Observable(observer => { 
      this.socket.on('user left', (data) => {
        console.log(data + ' joined');
        observer.next(data);
      });
      return () => {this.socket.disconnect();}; 
    })
    return observable;
  }

  public notify(pseudo) {
    console.log("sent 'add user' event : " + pseudo)
    this.socket.emit('add user', pseudo);
    this.pseudo = pseudo;
  }

  public attaquer(guest){
    if(this.pseudo == undefined){
      this.notifier.notify("warning", "You must login to battle !");
      return false;
    } else{
      this.socket.emit('battle me', this.pseudo, guest);
      this.notifier.notify("success", "Challenge sent !");
      console.log("challenge sent");
      return true;
    } 
  }

  public challengeAccepted(challenger){
    if(this.pseudo == undefined){
      this.notifier.notify("warning", "You must login to battle !");
      return false;
    } else{
      this.socket.emit('challenge accepted', this.pseudo, challenger); // nouvel événement .....
      this.notifier.notify("success", "Challenge sent !");
      console.log("challenge sent");
      return true;
    } 
  }

  public recevoirChallengeAccepted(){
    let observable = new Observable(observer => { 
      this.socket.on('challenge accepted', (data) => {
        console.log("challenge accepted " + data);
        observer.next(data);
      });
      return () => {this.socket.disconnect();}; 
    })
    return observable;
  }

  public recevoirChallenge(){
    let observable = new Observable(observer => { 
      this.socket.on('demande combat', (data) => {
        console.log("demande de combat " + data);
        observer.next(data);
      });
      return () => {this.socket.disconnect();}; 
    })
    return observable;
  }

  public recevoirListe(){
    this.socket.emit('get active users');
    let observable = new Observable(observer => { 
      this.socket.on('send list', (data) => {
        console.log("received list" + data);
        observer.next(data);
      });
      return () => {this.socket.disconnect();}; 
    })
    return observable;
  }

  public listenBattleExchange(){
    let observable = new Observable(observer => { 
      this.socket.on('personal battle data', (data) => {
        console.log("received battle data" + data);
        observer.next(data);
      });
      return () => {this.socket.disconnect();}; 
    })
    return observable;
  }

  public sendBattleExchcange(to, data){
    this.socket.emit('battle data', to, data);
    console.log("battle data sent");
  }

}
