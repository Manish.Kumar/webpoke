import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PokedexMenuComponent } from './pokedex-menu/pokedex-menu.component';
import { TeamMenuComponent } from './team-menu/team-menu.component';
import { BattleMenuComponent } from './battle-menu/battle-menu.component';
import { FriendsMenuComponent } from './friends-menu/friends-menu.component';
import { AccountMenuComponent } from './account-menu/account-menu.component';
import { ProfilComponent } from './profil/profil.component';
import { HomeComponent } from './home/home.component'
import { BattleComponent } from "./battle/battle.component";
import { LeaderboardComponent } from './leaderboard/leaderboard.component';


const routes: Routes = [
  {
    path: 'pokedex',
    component: PokedexMenuComponent
  },
  {
    path: 'team',
    component: TeamMenuComponent
  },
  {
    path: 'battle-menu',
    component: BattleMenuComponent
  },
  {
    path: 'friends',
    component: FriendsMenuComponent
  },
  {
    path: 'account',
    component: AccountMenuComponent
  },
  {
    path: 'profil',
    component: ProfilComponent
  },
  {
    path: 'battle/:id/:id2/:id3',
    component: BattleComponent
  },
  {
    path: '',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
