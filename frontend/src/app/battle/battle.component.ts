import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ActivatedRoute, Router } from '@angular/router';
import { ChatService } from '../chat.service';
@Component({
  selector: 'app-battle',
  templateUrl: './battle.component.html',
  styleUrls: ['./battle.component.css']
})
export class BattleComponent implements OnInit {

  maxHealth: number;
  curHealthSelf: number;
  pseudo: string;
  uid: any;
  log: boolean;
  level = 50;
  atk: number;
  typeMoi: number;
  typeMove: number;
  effet: any;
  selfTeam: any = [];
  curPoke: any = {};
  def: number;
  affichageHp: number = 0;
  dmgClasse: number;
  atkspec: number;

  enemyPokemon: any = {};
  enemyTeam: any = [];
  enemyType: number;
  enemyCurHealth = 100;
  enemyMaxHealth: number;
  enemyDef: number;
  enemySpecDEf: number;
  enemyAtk: number;
  enemyAffichageHp: number = 0;
  tempPoke: any; //to alternate between two pokemon when death
  oldPoke: any; //to deactivate when death

  enemyPseudo: string;
  endgame: boolean = false;
  displayEndgame: number = 0;
  tempPokemon: any;
  moveToSend: string;
  dmgToSend: number;

  private sub: any;
  private id_partie: number;
  joueurTour: number;
  yourTurn: boolean = false;
  waitingMsg: string = "Waiting for the enemy...";
  boxMessage: string = "Waiting for the enemy...";
  curPokeIndex: number;
  curEnemyPokeIndex: number;

  constructor(private router: Router, private route: ActivatedRoute, private httpClient: HttpClient, private chatService : ChatService) { }

  startup(){
    this.selfTeam.forEach( (el, i) => {
      if(el.active == 1) {
        this.curPoke = el;
        this.curPokeIndex = i;
        console.log(i);
      }
    });
    this.curHealthSelf = this.curPoke.stats[0].stat_value;
    this.atk = this.curPoke.stats[1].stat_value;
    this.def = this.curPoke.stats[2].stat_value;
    this.atkspec = this.curPoke.stats[4].stat_value;
    this.typeMoi = this.curPoke.types[0].id_type;
    this.maxHealth = this.curPoke.base_stats[0].base_stat;
    this.affichageHp = this.healthUpdate(this.curHealthSelf, this.maxHealth);
    this.enemyTeam.forEach( (el, i) => {
      if(el.active == 1) {
        this.enemyPokemon = el;
        this.curEnemyPokeIndex = i;
        console.log(i);
      }
    });
    this.enemyCurHealth = this.enemyPokemon.stats[0].stat_value;
    this.enemyAtk = this.enemyPokemon.stats[1].stat_value;
    this.enemyDef = this.enemyPokemon.stats[2].stat_value;
    this.enemySpecDEf = this.enemyPokemon.stats[5].stat_value;
    this.enemyType = this.enemyPokemon.types[0].id_type;
    this.enemyMaxHealth = this.enemyPokemon.base_stats[0].base_stat;
    this.enemyAffichageHp = this.healthUpdate(this.enemyCurHealth, this.enemyMaxHealth);
  }

  getNextPokemon(nextPokemon){
    this.curPoke = nextPokemon;
    this.curHealthSelf = this.curPoke.stats[0].stat_value;
    this.atk = this.curPoke.stats[1].stat_value;
    this.def = this.curPoke.stats[2].stat_value;
    this.typeMoi = this.curPoke.types[0].id_type;
    this.maxHealth = this.curPoke.base_stats[0].base_stat;
    this.affichageHp = this.healthUpdate(this.curHealthSelf, this.maxHealth);
  }

  getNextEnemyPokemon(){
    this.endgame = true;
    this.oldPoke = this.enemyPokemon
    this.enemyTeam.forEach(el => {
      if(el.stats[0].stat_value > 0) {
        this.tempPoke = el;
        this.endgame = false;
      }
      else this.endgame = this.endgame && true;
    });
    if(this.endgame){
      console.log("this is the end, you won !");
      setTimeout(() => this.displayEndgame = 1, 3000);
      setTimeout(() => this.boxMessage = "You won !", 3000);
      return false;
    } else{
      this.selfKOMessage(this.tempPoke);
      return true;
    }
  }

  selfKOMessage(tempPoke){
    setTimeout(() => this.boxMessage = this.enemyPokemon.pokeName + " is K.O. !", 2000);
    setTimeout(() => this.boxMessage = this.enemyPseudo + " summons " + tempPoke.pokeName + " !", 4000);
    setTimeout(() => this.affectEnemyStats(tempPoke), 5000);
    setTimeout(() => this.boxMessage = this.waitingMsg, 7000);
  }

  affectEnemyStats(tempPoke){
    this.enemyPokemon = tempPoke;
    this.enemyCurHealth = this.enemyPokemon.stats[0].stat_value;
    this.enemyAtk = this.enemyPokemon.stats[1].stat_value;
    this.enemyDef = this.enemyPokemon.stats[2].stat_value;
    this.enemyType = this.enemyPokemon.types[0].id_type;
    this.enemyMaxHealth = this.enemyPokemon.base_stats[0].base_stat;
    this.enemyAffichageHp = this.healthUpdate(this.enemyCurHealth, this.enemyMaxHealth);
  }

  healthUpdate(currentHealth, maxHealth){
    return (100*currentHealth)/maxHealth;
  }

  applyDamageSelf(dmg){
    this.curHealthSelf -= dmg;
    if(this.curHealthSelf < 0) this.curHealthSelf = 0;
    this.affichageHp = this.healthUpdate(this.curHealthSelf, this.maxHealth);
    this.updatePokemonJSON();
  }

  applyDamageOpponent(dmg){
    this.enemyCurHealth -= dmg;
    if(this.enemyCurHealth < 0) {
      this.enemyCurHealth = 0;
      this.updateDBPokemon(true);
    } else{
      this.updateDBPokemon(false);
    }
    setTimeout(() => this.enemyAffichageHp = this.healthUpdate(this.enemyCurHealth, this.enemyMaxHealth), 1000);
  }

  getCombatTeam(){
    this.httpClient.post("http://localhost:3000/getCombatTeam", {id_partie: this.id_partie}, { responseType: 'json', withCredentials: true }).subscribe(
      (res: any) => {
        console.log(res);
        this.selfTeam = res.self;
        this.enemyTeam = res.enemy;
        this.startup();
    });
  }

  typeAdv(move){
    console.log(move);
    this.httpClient.post("http://localhost:3000/geteff", { typeNous: move.m_id_type, typeEux: this.enemyType}, {responseType: 'json', withCredentials: true}).subscribe((res:any) => {
      console.log(res); 
      this.moveToSend = move;
      this.boxMessage = this.curPoke.pokeName + " used " + move.identifier + " !";
      this.dmgClasse = move.id_dmg_classe;
      this.dmgToSend = this.calculeDmg(res.body[0].damage_factor, move.power); 
      move.pp--; 
      this.yourTurn = false;
    });
  }

  updateDBPokemon(death){
    this.updatePokemonJSON()
    if(death){
      this.enemyPokemon.active = 0;
      if(this.getNextEnemyPokemon()){
        console.log("death note");
        this.httpClient.post("http://localhost:3000/updatepartie", {nextPokemon: this.tempPoke.id_etat, pokemon1: this.curPoke, pokemon2: this.oldPoke, id_partie: this.id_partie, winner: undefined}, {responseType: 'json', withCredentials: true}).subscribe((res:any) => {
          console.log(res);
          this.chatService.sendBattleExchcange(this.enemyPseudo, {move: this.moveToSend, dmg: this.dmgToSend, nextPokemon: this.tempPoke, endgame: false, sender: this.pseudo});
        });
      } else{
        console.log("winning note");
        this.httpClient.post("http://localhost:3000/updatepartie", {nextPokemon: -1, pokemon1: this.curPoke, pokemon2: this.oldPoke, id_partie: this.id_partie, winner: this.uid}, {responseType: 'json', withCredentials: true}).subscribe((res:any) => {
          console.log(res);
          this.chatService.sendBattleExchcange(this.enemyPseudo, {move: this.moveToSend, dmg: this.dmgToSend, nextPokemon: this.enemyPokemon, endgame: true, sender: this.pseudo});
        });
      }
    } else{
      console.log("live note");
      this.httpClient.post("http://localhost:3000/updatepartie", {nextPokemon: -1, pokemon1: this.curPoke, pokemon2: this.enemyPokemon, id_partie: this.id_partie, winner: undefined}, {responseType: 'json', withCredentials: true}).subscribe((res:any) => {
        console.log(res);
        this.chatService.sendBattleExchcange(this.enemyPseudo, {move: this.moveToSend, dmg: this.dmgToSend, nextPokemon: undefined, endgame: false, sender: this.pseudo});
      });
      setTimeout(() => this.boxMessage = this.waitingMsg, 1000);
    }
  }

  updatePokemonJSON(){
    this.enemyPokemon.stats[0].stat_value = this.enemyCurHealth;
    this.enemyPokemon.stats[1].stat_value = this.enemyAtk;
    this.enemyPokemon.stats[2].stat_value = this.enemyDef;
    this.curPoke.stats[0].stat_value = this.curHealthSelf;
    this.curPoke.stats[1].stat_value = this.atk;
    this.curPoke.stats[2].stat_value = this.def;
    this.selfTeam[this.curPokeIndex] = this.curPoke;
    this.enemyTeam[this.curEnemyPokeIndex] = this.enemyPokemon;
  }

  calculeDmg(eff, power){
    console.log(eff, power, this.dmgClasse);
    console.log(this.enemyDef + " " + this.atk);
    if(this.dmgClasse == 2){
      let dmg = Math.round(((((0.4*this.level+2)*this.atk*power)/(50*this.enemyDef))+2)*(eff/100)*(Math.random()*(1 - 0.85)+ 0.85));
      console.log(this.level, this.atk, power, this.enemyDef, )
      console.log(dmg + " " + this.enemyCurHealth);
      this.applyDamageOpponent(dmg);
      return dmg;
    } else if (this.dmgClasse == 3){
        let dmg = Math.round(((((0.4*this.level+2)*this.atkspec*power)/(50*this.enemySpecDEf))+2)*(eff/100)*(Math.random()*(1 - 0.85)+ 0.85));
        this.applyDamageOpponent(dmg);
        console.log(dmg + " " + this.enemyCurHealth);
        return dmg;
    } else {
      this.applyDamageOpponent(0);
      return 0;
    }
  }

  ngOnInit(): void {
    this.httpClient.get("http://localhost:3000/checkLogin", { responseType: 'json', withCredentials: true}).subscribe( (res: any) => {
      this.log = res.res;
      this.pseudo = res.body;
      this.uid = res.uid;
      if(this.joueurTour == this.uid) this.yourTurn = true;
      console.log(this.joueurTour, this.uid, this.yourTurn, this.joueurTour==this.uid);
      console.log(res);
      this.getCombatTeam();
    });
    this.sub = this.route.params.subscribe(params => {
      this.id_partie = +params['id']; // (+) converts string 'id' to a number
      this.joueurTour = +params['id2'];
      this.enemyPseudo = params['id3'];
      console.log(this.id_partie + " " + this.joueurTour + " " + this.enemyPseudo);
    });
    this.chatService.listenBattleExchange().subscribe((data: any) => {
      if(data.sender == this.enemyPseudo){
        console.log(data);
        this.boxMessage = this.enemyPokemon.pokeName + " used " + data.move.identifier + " !";
        setTimeout(() => this.applyDamageSelf(data.dmg), 2000);
        if(data.endgame){
          setTimeout(() => this.boxMessage = "You lost", 5000);
          setTimeout(() => this.displayEndgame = 2, 5000);
        } else{
          if(data.nextPokemon != undefined){
            setTimeout(() => this.getNextPokemon(data.nextPokemon), 5000);
          }
          setTimeout(() => this.yourTurn = true, 6000);
        }
      }
    });
  }

}
