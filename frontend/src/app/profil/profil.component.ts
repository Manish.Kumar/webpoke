import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AccountSyncService } from "../services/account-sync/account-sync.service";

@Component({
  selector: 'app-profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {
  user: String;
  res: any[];
  constructor(private httpClient: HttpClient, private router: Router, private accountData: AccountSyncService) { }

  logout(){
    this.httpClient.get("http://localHost:3000/logout", { withCredentials: true}).subscribe( (res => { console.log(res) }));
    this.accountData.updateUsername("Account"); 
    this.router.navigate(['/pokedex']);
  }

  ngOnInit(): void {
   
  }

}
