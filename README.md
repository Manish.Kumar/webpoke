# WebPoke

[Video demonstration](https://drive.google.com/file/d/1LFv3bqg8pnRlWZAzWawTdYFEy3DQFT4X/preview)

## First time launching the project

1. First activate your Unige VPN, as the backend currently automatically connects to the database.\
\
You will need to run the next commands from the main `webpoke` folder:

2. Type this command `npm install --save-dev concurrently` to install a module enabling concurrent commands execution for the scripts shortcuts in the `package.json`
3. Then you have to execute `npm run installAll` which will install the node modules in the backend and frontend at the same time. So just wait a little while everything is installed, it can take some time.
3. Finally run `npm run dev` to launch the backend and frontend simultaneously. Once you see that the backend is listening on both ports 3000/3001 and that Angular is live on port 4200, you can go to the following address `/localhost:4200` on two different windows to test the temporary chat feature.

## launching the project after a pull

Make sure to run `npm run installAll` to install all the new packages after which you can run the project normally with `npm run dev`.


## Extra features

The GET request displaying all pokemons on the database can be accessed on this address : `http://localhost:3000/allpokemon`
